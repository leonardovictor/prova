package com.example.pdmprova2.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;

public class Registro implements Parcelable {
    private String tipoTransacao ;
    private String tituloMovimentacao;
    private BigDecimal valor;

    public Registro(String tipoTransacao, String tituloMovimentacao, BigDecimal valor){
        this.tipoTransacao = tipoTransacao;
        this.tituloMovimentacao = tituloMovimentacao;
        this.valor = valor;
    }

    public String getTipoTransacao() {
        return tipoTransacao;
    }

    public String getTituloMovimentacao() {
        return tituloMovimentacao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tipoTransacao);
        dest.writeString(tituloMovimentacao);
    }

    protected Registro(Parcel in) {
        tipoTransacao = in.readString();
        tituloMovimentacao = in.readString();
    }

    public static final Creator<Registro> CREATOR = new Creator<Registro>() {
        @Override
        public Registro createFromParcel(Parcel in) {
            return new Registro(in);
        }

        @Override
        public Registro[] newArray(int size) {
            return new Registro[size];
        }
    };
}
