package com.example.pdmprova2.adapter;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pdmprova2.R;
import com.example.pdmprova2.model.Registro;

import java.math.BigDecimal;
import java.util.List;

public class AdapterRegistro extends RecyclerView.Adapter<AdapterRegistro.MyViewHolder> {

    private List<Registro> registros;

    public AdapterRegistro(List<Registro> registros){
        this.registros = registros;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_registro_view, parent, false);

        return new MyViewHolder(itemLista);
    }

    @Override
    public void onBindViewHolder( AdapterRegistro.MyViewHolder holder, int position) {

        Registro registro = registros.get(position);

        holder.tituloMovimentacao.setText(registro.getTituloMovimentacao());
        holder.valor.setText("R$" + registro.getValor());

        if ( registro.getTipoTransacao() == "Débito") {
            holder.tipoTransacao.setBackgroundColor(Color.parseColor("#e57373"));
        }

    }

    @Override
    public int getItemCount() {

        return registros.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tipoTransacao ;
        TextView tituloMovimentacao;
        TextView valor;

        public MyViewHolder(View itemView) {

            super(itemView);

            tipoTransacao = itemView.findViewById(R.id.txvTipo);
            tituloMovimentacao = itemView.findViewById(R.id.txvTitulo);
            valor = itemView.findViewById(R.id.txvValor);
        }
    }

}
