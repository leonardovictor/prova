package com.example.pdmprova2.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.pdmprova2.R;
import com.example.pdmprova2.adapter.AdapterRegistro;
import com.example.pdmprova2.model.Registro;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Registro> registros = new ArrayList<>();;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);

        // Lista de registros
        this.createRegistros();

        // Configurar Adpter
        AdapterRegistro adapter = new AdapterRegistro(registros);

        // Configurar RecyclerView
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration( new DividerItemDecoration(this, LinearLayout.VERTICAL));
        recyclerView.setAdapter( adapter );
    }

    public void createRegistros(){
        Registro registro = new Registro("Débito", "Compra no supermercado", new BigDecimal("100.0"));
        this.registros.add(registro);

        registro = new Registro("Crédito", "Compra no supermercado", new BigDecimal("500.0"));
        this.registros.add(registro);

        registro = new Registro("Débito", "Compra de passagens aéreas", new BigDecimal("160.0"));
        this.registros.add(registro);

        registro = new Registro("Débito", "Compra de medicamentos", new BigDecimal("20.0"));
        this.registros.add(registro);

        registro = new Registro("Crédito", "Depósito", new BigDecimal("100.0"));
        this.registros.add(registro);

        registro = new Registro("Crédito", "Depósito", new BigDecimal("300.0"));
        this.registros.add(registro);

        registro = new Registro("Débito", "Pagamento de conta de luz", new BigDecimal("360.0"));
        this.registros.add(registro);

        registro = new Registro("Crédito", "Transferência bancária", new BigDecimal("60.0"));
        this.registros.add(registro);

        registro = new Registro("Débito", "Compra no supermercado", new BigDecimal("15.0"));
        this.registros.add(registro);

        registro = new Registro("Débito", "Compra no supermercado", new BigDecimal("499.0"));
        this.registros.add(registro);
    }

//    public void onClickListRegistros(View v){
//        Intent listRegistros = new Intent(this, ListRegistrosActivity.class);
//    }
    
    
}